import React from 'react';
import { ICurrency } from './interfaces';


interface IProps {
  currency: ICurrency;
}

const Currency: React.FunctionComponent<IProps> = ({ currency }: IProps) => {
  const { id, name, min_size } = currency;
  return (
    <>
      <li>
        <p>
          <span>{id || '-'}</span>
          <br />
          <span>{name || ''}</span>
          <br />
          <span>{min_size || ''}</span>
          <br />
        </p>
      </li>
    </>
  );
}
export { Currency };