interface ICurrency {
  id: string;
  min_size: string;
  name: string;
}

export type { ICurrency };