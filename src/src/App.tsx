import React from 'react';
import './App.css';
import FetchExample from './fetch-example';
import SimpleForm from './form-example';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <SimpleForm />
        <FetchExample />
      </header>
    </div>
  );
}

export default App;
