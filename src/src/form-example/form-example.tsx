import React from 'react';

interface IState {
  input: string;
}

const initialState: IState = {
  input: ""
};


// Компонент класс без вмешательства в метод жизненного цикла shouldComponentUpdate 
class SimpleForm extends React.PureComponent<{}, IState> {

  constructor({ }) {
    super({});
    this.state = initialState;
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  // Arrow function expression method
  public handleInputChange = (ev: React.ChangeEvent<HTMLInputElement>): void => {
    ev.preventDefault();

    // синхронное изменение стейта
    this.setState({
      input: ev.target.value.toString()
    });
  }

  // Default method declaration
  public handleFormSubmit(ev: React.FormEvent<HTMLFormElement>): void {
    ev.preventDefault();
    alert(`On submit ${this.state.input}`);
    this.setState(initialState);
  }

  render() {
    return (

      <div className='form__example'>
        <form onSubmit={this.handleFormSubmit}>
          <label>
            Input
            <input type='text' value={this.state.input} onChange={this.handleInputChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }

}

export { SimpleForm };