import React, { useState } from 'react'
import Currency, { ICurrency } from '../currency';
import axios from 'axios'

// Functional component with useState hook
const FetchExample: React.FunctionComponent = () => {
  const [currencies, setValue] = useState<Array<ICurrency>>([]);

  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    axios.get("/currencies")
      .then(response => {
        // console.log(response);
        return response.data;

      })
      .then(value => {
        console.log(value)
        setValue(Array.from(value["data"]));
      })
      .catch(err => {
        console.error(err);
      });
  }
// map and conditional rendering example 
  return (
    <div>
      <button onClick={handleButtonClick}>FETCH</button>
      <ol>
        {currencies && currencies.length > 0 &&

          currencies.map((cur, i) => <Currency
            key={`${cur.id}_${i}`}
            currency={cur}
          />)
        }
      </ol>
    </div>
  );

}

export { FetchExample }